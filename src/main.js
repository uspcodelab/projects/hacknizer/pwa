import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import apolloProvider from './apollo';
import store from './store';
import './registerServiceWorker';

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";

library.add(faGitlab);

Vue.component("fas", FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount('#app');
