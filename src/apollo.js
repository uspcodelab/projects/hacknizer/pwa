import Vue from 'vue';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';

const baseAPIUrl = 'http://localhost:3000/graphql';

const cache = new InMemoryCache();

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  };
});

const httpLink = new HttpLink({
  uri: baseAPIUrl
});

const link = ApolloLink.from([authLink, httpLink]);

// Create Apollo client
const apolloClient = new ApolloClient({
  link,
  cache,
  connectToDevTools: true,
  defaultOptions: {
    watchQuery: {
      errorPolicy: 'all',
      fetchPolicy: 'no-cache'
    },
    query: {
      errorPolicy: 'all',
      fetchPolicy: 'no-cache'
    },
    mutation: {
      errorPolicy: 'all',
      fetchPolicy: 'no-cache'
    }
  }
});

export default new VueApollo({
  defaultClient: apolloClient
});

// Install to Vue
Vue.use(VueApollo);
